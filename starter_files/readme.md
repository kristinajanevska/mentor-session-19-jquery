# Brainster Weekly Challenge 19 - x / o game

## Info/Sending:

1. Place the challenge in a GitLab project named **mentor-classes-group-number** (example: mentor-classes-group-1).
   1. Each challenge every week will be in a separate branch (DO NOT WORK ON MASTER).
   2. Name your branch - **weekly-challenge-1-paginaton**
   3. You will get the solution afterwards.
2. You have 1.5/2 hours to divide your work & finish this challenge.

## Tasks:

You'll have to create x / o game in jQuery:

1. HTML and CSS are ready, no need to change those.
2. All your code should go into main.js

## Hints:

1. Think before you google!
2. Use global variables to keep track of who is current player
3. For given sign there are only 8 possible combinations of winning the game, (dont overcomplicate)
4. Reset functionality should be available at all time. And it should reset only the board (not score)
5. Score table is updated when user wins the game